<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H5';

$current_user_parent_role_seq='H1::H2::H7::H5';

$current_user_profiles=array(2,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>1,'2'=>0,'3'=>0,'4'=>0,'6'=>0,'7'=>0,'8'=>1,'9'=>0,'10'=>0,'13'=>0,'14'=>0,'15'=>1,'16'=>0,'18'=>1,'19'=>1,'20'=>1,'21'=>0,'22'=>0,'23'=>0,'24'=>0,'25'=>1,'26'=>0,'27'=>1,'30'=>0,'31'=>0,'32'=>1,'33'=>0,'34'=>1,'35'=>1,'36'=>0,'37'=>0,'38'=>0,'39'=>1,'40'=>1,'41'=>1,'42'=>1,'43'=>1,'44'=>1,'45'=>0,'46'=>1,'47'=>0,'48'=>0,'49'=>0,'28'=>0,);

$profileActionPermission=array(2=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>1,6=>1,10=>0,),4=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>1,6=>1,8=>0,10=>0,),6=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>1,6=>1,8=>0,10=>0,),7=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>1,6=>1,8=>0,9=>0,10=>0,),8=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,6=>1,),9=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>0,6=>0,),13=>array(0=>1,1=>1,2=>1,3=>0,4=>0,7=>1,5=>1,6=>1,8=>0,10=>0,),14=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>1,6=>1,10=>0,),15=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),16=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),18=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>1,6=>1,10=>0,),19=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>1,6=>1,10=>0,),20=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,),21=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>0,6=>0,),22=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>0,6=>0,),23=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,5=>0,6=>0,),25=>array(0=>0,1=>0,2=>0,4=>0,7=>0,6=>0,13=>0,),26=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),32=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,10=>0,),34=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,10=>0,),35=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,11=>0,12=>0,),39=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),40=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,10=>0,),41=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,10=>0,),42=>array(0=>1,1=>1,2=>1,3=>0,4=>1,7=>1,5=>0,6=>0,10=>0,),43=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),47=>array(0=>0,1=>0,2=>0,3=>0,4=>0,7=>0,),);

$current_user_groups=array(2,6,);

$subordinate_roles=array();

$parent_roles=array('H1','H2','H7',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'vendedor10','is_admin'=>'off','user_password'=>'$1$ve000000$ALZKukWRWgNvJXv7XxPee.','confirm_password'=>'$1$ve000000$ALZKukWRWgNvJXv7XxPee.','first_name'=>'Milton','last_name'=>'Correa','roleid'=>'H5','email1'=>'vendedor10@gmail.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'12','end_hour'=>'','start_hour'=>'09:00','is_owner'=>'','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'yyyy-mm-dd','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'QhhK2cYHLtOk9ZPP','time_zone'=>'America/Argentina/Buenos_Aires','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>',','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'es_es','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Sunday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'medium','defaulteventstatus'=>'','defaultactivitytype'=>'','hidecompletedevents'=>'0','defaultcalendarview'=>'','currency_name'=>'Argentina, Pesos','currency_code'=>'ARS','currency_symbol'=>'&#36;','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'7');
?>